from datetime import datetime
import sqlite3
from sqlite3 import Error
import time


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        # print(sqlite3.version)
    except Error as e:
        print(e)
    # finally:
    #     if conn:
    #         conn.close()
    return conn


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def add_Task(conn):
    task_name = input("Write task name: ")

    sql = """ INSERT INTO tasks (name, parent, duration) VALUES (?,?,?) """
    cur = conn.cursor()
    cur.execute(sql, (task_name, 0, 0))
    conn.commit()
    cur.close()
    return task_name


def list_tasks(conn):
    sql = """ SELECT id, name FROM tasks ORDER BY id ASC """
    cur = conn.cursor()
    cur.execute(sql)
    tasks = cur.fetchall()
    print("Existing tasks:")
    for task in tasks:
        print(f"{task[0]}. {task[1]}")


def task_choice(conn):
    while True:
        list_tasks(conn)
        task_choice = input(
            "Select existing, add new or delete task (num/add/del):")
        if task_choice == "add":
            add_Task(conn)
        elif task_choice == "del":
            id = int(input("Enter the task number to be deleted: "))
            delete_task(conn, id)
        elif task_choice.isnumeric():
            cur = conn.cursor()
            cur.execute("SELECT id, name FROM tasks WHERE id=?",
                        (int(task_choice),))
            task = cur.fetchall()
            # print(f"Your chosen task is: {task[0][1]}")
            chosen_task = task[0][1]
            return chosen_task


def write_time(name, start, stop, duration, conn):
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO timer (name,start_time,end_time,duration) VALUES (?,?,?,?)",
        (name, start, stop, duration),
    )
    conn.commit()


def delete_task(conn, id):
    cur = conn.cursor()
    cur.execute("DELETE FROM tasks WHERE id=?", (id,))
    conn.commit()


def main():
    database = "tt.db"

    sql_create_task_table = """ CREATE TABLE IF NOT EXISTS tasks (
        id integer PRIMARY KEY,
        parent integer,
        name text NOT NULL UNIQUE,
        duration integer
    ); """

    sql_create_time_table = """CREATE TABLE IF NOT EXISTS timer(
        id integer PRIMARY KEY,
        name text NOT NULL,
        start_time text NOT NULL,
        end_time text NOT NULL,
        duration integer NOT NULL
    );"""

    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_task_table)

        # create tasks table
        create_table(conn, sql_create_time_table)
    else:
        print("Error! cannot create the database connection.")

    task_in_use = task_choice(conn)
    while True:
        print(f"Active task: {task_in_use}")
        choice = input("start/list/change/end: ")
        match choice:
            case "start":
                start = datetime.now()
                print("Press CTRL + C to stop timer")
                while True:

                    try:
                        duration = (datetime.now() - start).seconds
                        hour = duration // 3600
                        seconds = duration % 3600
                        minutes = seconds // 60
                        seconds %= 60
                        
                        print(
                            f"\rWork on '{task_in_use}' - {hour:02d}:{minutes:02d}:{seconds:02d} ", end="", flush=True)
                        
                        time.sleep(1)
                    except KeyboardInterrupt:  # Ctrl + C to stop
                        print(" Timer stopped")
                        break
                stop = datetime.now()

                start_time = start.strftime("%Y:%m:%d %H:%M:%S")
                stop_time = stop.strftime("%Y:%m:%d %H:%M:%S")
                czas_trwania = (stop - start).seconds
                write_time(task_in_use, start_time,
                           stop_time, czas_trwania, conn)

            case "end":
                break
            case "list":
                list_tasks(conn)
            case "change":
                task_in_use = task_choice(conn)
            case _:
                print('Wrong command, try again')


if __name__ == "__main__":
    print("Hello, this is time tracker")
    # create_connection("tt.db")
    main()
