from datetime import datetime
import sqlite3
from sqlite3 import Error
import time
import curses


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        
    except Error as e:
        print(e)
   
    return conn


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def add_Task(conn, stdsrc):
    curses.echo()
    stdsrc.move(1,1)
    stdsrc.clrtoeol()
    stdsrc.addstr(1,1,"Enter task name: ")
    task_name = stdsrc.getstr().decode('utf-8')
    
    sql = """ INSERT INTO tasks (name, parent, duration) VALUES (?,?,?) """
    cur = conn.cursor()
    cur.execute(sql, (task_name, 0, 0))
    conn.commit()
    cur.close()
    curses.noecho()
    return task_name


def list_tasks(conn, stdsrc, mv):
    sql = """ SELECT id, name FROM tasks ORDER BY id ASC """
    cur = conn.cursor()
    cur.execute(sql)
    tasks = cur.fetchall()
    crate_main_window(stdsrc)
   
    task_win = stdsrc.subwin(20, 60, 4, 4)
    task_win.border()
    task_win.addstr(0,3,f" Existing tasks: {len(tasks)} ")
    #task_win.addstr(1,0,f"Existing tasks: {mv}\n")
    if mv < 0:
        mv = 0
    taski = tasks[mv:mv+15]
    for i, task in enumerate(taski):
         task_win.addstr(i + 1, 2, f"{task[0]}. {task[1]}")
    task_win.refresh()
    stdsrc.refresh()
    return tasks

def task_choice(conn, stdsrc):
    mv =0 
    while True:
        tasks = list_tasks(conn, stdsrc, mv)
        task_nums = [i[0] for i in tasks]
            
        print("\n")
        curses.echo()
        stdsrc.addstr(1,2,"Select existing, add new or delete task (num/(a)dd/(d)el)/scroll(j/k):")
        task_choice = stdsrc.getstr().decode('utf-8')
        curses.noecho()
        # stdsrc.move(1,1)
        # stdsrc.clrtoeol()
        if task_choice == "a":
            
            add_Task(conn, stdsrc)
            mv = len(tasks) - 10
            list_tasks(conn, stdsrc, mv)
        elif task_choice == "j":
            mv = mv + 10
            list_tasks(conn, stdsrc, mv)
        elif task_choice == "k":
            mv = mv - 10
            list_tasks(conn, stdsrc, mv)
        elif task_choice == "d":
            curses.echo()
            stdsrc.move(1,1)
            stdsrc.clrtoeol()
            stdsrc.addstr("Enter the task number to be deleted: ")
            id = int(stdsrc.getstr().decode('utf-8'))
            delete_task(conn, id)
        elif task_choice.isnumeric() and int(task_choice) in task_nums:
            
            cur = conn.cursor()
            cur.execute("SELECT id, name FROM tasks WHERE id=?",
                        (int(task_choice),))
            task = cur.fetchall()
            
            # print(f"Your chosen task is: {task[0][1]}")
            chosen_task = task[0][1]
            return chosen_task
        
            

def write_time(name, start, stop, duration, conn):
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO timer (name,start_time,end_time,duration) VALUES (?,?,?,?)",
        (name, start, stop, duration),
    )
    conn.commit()
    old_dur = cur.execute(""" SELECT duration FROM tasks WHERE name=?""", (name,))
    cur.execute(""" UPDATE tasks SET duration=? WHERE name=? """, (duration + old_dur.fetchone()[0], name))
    conn.commit()


def delete_task(conn, id):
    
    cur = conn.cursor()
    cur.execute("DELETE FROM tasks WHERE id=?", (id,))
    conn.commit()

def crate_main_window(stdsrc):
    stdsrc.clear()
    stdsrc.border()
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    RED_AND_WHITE = curses.color_pair(1)
    num_rows, num_cols = stdsrc.getmaxyx()
    middle_column = int(num_cols/ 2)

    stdsrc.addstr(0, middle_column - 10, "This is Time Tracker", RED_AND_WHITE | curses.A_BOLD)
    stdsrc.refresh()

def main(stdsrc):
    #stdsrc.nodelay(True)
    curses.curs_set(1)
    crate_main_window(stdsrc)
    
    database = "tt.db"

    sql_create_task_table = """ CREATE TABLE IF NOT EXISTS tasks (
        id integer PRIMARY KEY,
        parent integer,
        name text NOT NULL UNIQUE,
        duration integer
    ); """

    sql_create_time_table = """CREATE TABLE IF NOT EXISTS timer(
        id integer PRIMARY KEY,
        name text NOT NULL,
        start_time text NOT NULL,
        end_time text NOT NULL,
        duration integer NOT NULL
    );"""

    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_task_table)

        # create tasks table
        create_table(conn, sql_create_time_table)
    else:
        print("Error! cannot create the database connection.")

    task_in_use = task_choice(conn, stdsrc)
    # stdsrc.erase()
    crate_main_window(stdsrc)
    # stdsrc.refresh()
    while True:
        # crate_main_window(stdsrc)
        # # stdsrc.refresh()
        twin = stdsrc.subwin(5, 50, 2, 10)
        twin.border()
        twin.addnstr(0, 15,f" Active task: {task_in_use} ", 20)
        stdsrc.move(1,1)
        stdsrc.clrtoeol()
        stdsrc.addstr(1, 1,"(s)tart/(c)hange/(e)nd: ")
        
        twin.refresh()
        choice = stdsrc.getkey()
        match choice:
            case "s":
                stdsrc.move(1,1)
                stdsrc.clrtoeol()
                start = datetime.now()
                stdsrc.addstr(8, 10,"Press 'any' key to stop timer")
                stdsrc.refresh()
                while True:
                    stdsrc.nodelay(1)
                    
                    
                    duration = (datetime.now() - start).seconds
                    hour = duration // 3600
                    seconds = duration % 3600
                    minutes = seconds // 60
                    seconds %= 60
                    
                    twin.addstr(2,2,
                        f"Counting..        - {hour:02d}:{minutes:02d}:{seconds:02d} ")
                    
                    twin.refresh()
                    time.sleep(1)
                    if stdsrc.getch() >=0 :
                        twin.addstr(2,2," Timer stopped")
                        twin.refresh()
                        stdsrc.nodelay(False)
                        break
                stop = datetime.now()

                start_time = start.strftime("%Y:%m:%d %H:%M:%S")
                stop_time = stop.strftime("%Y:%m:%d %H:%M:%S")
                czas_trwania = (stop - start).seconds
                write_time(task_in_use, start_time,
                           stop_time, czas_trwania, conn)

            case "e":
                break
            case "c":
                stdsrc.clear()
                crate_main_window(stdsrc)
                task_in_use = task_choice(conn, stdsrc)
                crate_main_window(stdsrc)
                
            case _:
                print('Wrong command, try again')
        stdsrc.refresh()

if __name__ == "__main__":
    
    # create_connection("tt.db")
    curses.wrapper(main)
