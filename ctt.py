import click

while True:
    click.echo("Continue? [y/n] ", nl=False)
    c = click.getchar()
    click.echo("hey")
    if c == "y":
        click.echo("We will go on")
    elif c == "n":
        click.echo("Abort!")
        break
    else:
        click.echo("Invalid input")
