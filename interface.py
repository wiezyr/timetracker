import curses

print("Preparing to initialize screen..")
screen = curses.initscr()
print("Screen initialized")
screen.border()
curses.start_color()
curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
RED_AND_WHITE = curses.color_pair(1)
num_rows, num_cols = screen.getmaxyx()
middle_column = int(num_cols/ 2)

screen.addstr(0, middle_column - 10, "This is Time Tracker", RED_AND_WHITE | curses.A_BOLD)
# screen.idlok(True)
# for i in range(100):
#     screen.addstr(f"{i} linia\n")  
#     curses.napms(200)
    
screen.refresh()
task_pad = curses.newpad(100, 30)
for i in range(50):
    task_pad.addstr(f"{i} linia\n")  
    #curses.napms(200)
    
    task_pad.refresh(0,0,2,3,20,50)
scroll = screen.getkey()
if scroll == "l":
    task_pad.refresh(10,0,2, 3,20,50)
# task_pad.addstr("hello")
# task_pad.refresh(0,0,)
#curses.napms(2000)
screen.getch()
curses.endwin()
print("Window ended")